<?php

use App\Livewire\Dashboard;
use App\Livewire\Login;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::get('/', function () {
//     return view('admin.home');
// });

Route::middleware('guest')->group(function(){

    Route::get('/login', Login::class)->name('login');
    Route::get('/', Login::class);

});


Route::middleware('auth')->group(function(){

    Route::get('/home', Dashboard::class)->name('home');
    Route::get('/dashboard', Dashboard::class)->name('dashboard');

    // Route::get('/home', [App\Http\Controllers\Admin\DashboardController::class, 'index'])->name('home');
    // Route::get('/dashboard', [App\Http\Controllers\Admin\DashboardController::class, 'index'])->name('dashboard');

});

