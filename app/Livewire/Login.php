<?php

namespace App\Livewire;

use Livewire\Component;
use App\Livewire\Forms\LoginForm;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class Login extends Component
{
    public LoginForm $loginForm;

    public function render()
    {
        return view('livewire.login');
    }


    public function login()
    {
        $data = $this->loginForm->validate();

        $user = User::where('email', $data['email'])->first();

        if( $user && Auth::attempt([ 'email' => $data['email'], 'password'=> $data['password'] ], $data['remember']))
        {
            return $this->redirect('/dashboard');
        }

        $this->dispatch('swal', ['title'=> 'Oops!', 'text'=> 'Invalid Login credentials', 'icon'=> 'error']);

    }
}
