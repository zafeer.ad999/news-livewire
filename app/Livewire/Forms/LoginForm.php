<?php

namespace App\Livewire\Forms;

use Livewire\Attributes\Validate;
use Livewire\Form;

class LoginForm extends Form
{
    #[Validate('required|email|exists:users,email')]
    public $email = '';

    #[Validate('required|min:8')]
    public $password = '';

    #[Validate('nullable')]
    public $remember = '';
}
