<x-admin.layout>
    <x-slot name="title">Dashboard</x-slot>


    @livewire('dashboard')

    @push('scripts')

    @endpush
</x-admin.layout>
