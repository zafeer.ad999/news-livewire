<div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-7"><img class="bg-img-cover bg-center" src="{{ asset('admin/images/login/2.jpg') }}" alt="loginpage"></div>
            <div class="col-xl-5 p-0">
                <div class="login-card">
                    <div>
                        <div><a class="logo" href="index.html"><img class="img-fluid for-light" src="{{ asset('admin/images/logo/login.png') }}" alt="loginpage"></a></div>
                        <div class="login-main">

                            <form class="theme-form" wire:submit="login">

                                <h4 class="text-center">Sign in to account</h4>
                                <p class="text-center">Enter your email & password to login</p>
                                <div class="form-group">
                                    <label class="col-form-label">Email Address</label>
                                    <input class="form-control @error('loginForm.email') is-invalid @enderror" type="email" wire:model="loginForm.email" placeholder="Test@gmail.com">
                                    <div>
                                        @error('loginForm.email') <span class="text-danger">{{ $message }}</span> @enderror
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-form-label">Password</label>
                                    <div class="form-input position-relative">
                                        <input class="form-control @error('loginForm.password') is-invalid @enderror" type="password" wire:model="loginForm.password" name="login[password]" placeholder="*********">
                                        <div class="show-hide"><span class="show"> </span></div>
                                    </div>
                                    <div>
                                        @error('loginForm.password') <span class="text-danger">{{ $message }}</span> @enderror
                                    </div>
                                </div>

                                <div class="form-group mb-0">
                                    <div class="checkbox p-0">
                                        <input id="checkbox1" type="checkbox" wire:model="loginForm.remember">
                                        <label class="text-muted" for="checkbox1">Remember password</label>
                                    </div>
                                    <a class="link" href="forget-password.html">Forgot password?</a>
                                    <div class="text-end mt-3">
                                        <button class="btn btn-primary btn-block w-100" type="submit">Sign in </button>
                                    </div>
                                </div>

                                <div class="login-social-title">
                                    <h6>Or Sign in with </h6>
                                </div>
                                <div class="form-group">
                                    <ul class="login-social">
                                        <li><a href="https://www.linkedin.com/" target="_blank"><i data-feather="linkedin"></i></a></li>
                                        <li><a href="https://www.twitter.com/" target="_blank"><i data-feather="twitter"></i></a></li>
                                        <li><a href="https://www.facebook.com/" target="_blank"><i data-feather="facebook"></i></a></li>
                                        <li><a href="https://www.instagram.com/" target="_blank"><i data-feather="instagram"></i></a></li>
                                    </ul>
                                </div>
                                <p class="mt-4 mb-0 text-center">Don't have account?<a class="ms-2" href="sign-up.html">Create Account</a></p>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
