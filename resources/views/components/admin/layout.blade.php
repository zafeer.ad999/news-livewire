<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="{{ config('app.description') }}">
    <meta name="keywords" content="admin template, Enzo admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="pixelstrap">
    <link rel="icon" href="../assets/images/favicon/favicon.png" type="image/x-icon">
    <link rel="shortcut icon" href="../assets/images/favicon/favicon.png" type="image/x-icon">

    <title>{{ $title ?? 'Page Title' }}</title>


    <!-- Google font-->
    <link rel="preconnect" href="https://fonts.googleapis.com/">
    <link rel="preconnect" href="https://fonts.gstatic.com/" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Rubik:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,300;1,400;1,500;1,600;1,700;1,800;1,900&amp;display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&amp;display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&amp;display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('admin/css/vendors/font-awesome.css') }}">
    <!-- ico-font-->
    <link rel="stylesheet" type="text/css" href="{{ asset('admin/css/vendors/icofont.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admin/css/vendors/themify.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admin/css/vendors/flag-icon.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admin/css/vendors/feather-icon.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admin/css/vendors/bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admin/css/style.css') }}">
    <link id="color" rel="stylesheet" href="{{ asset('admin/css/color-1.css" media="screen') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admin/css/responsive.css') }}">
    @stack('styles')
</head>

<body>
    <div class="loader-wrapper">
        <div class="loader"></div>
    </div>

    <div class="page-wrapper compact-wrapper" id="pageWrapper">
        <x-admin.header />

        <div class="page-body-wrapper">

            <x-admin.sidebar />
            <div class="page-body">
                <div class="container-fluid default-dash">
                    <div class="row">

                        {{ $slot }}

                    </div>
                </div>
            </div>
            <x-admin.footer />

        </div>
    </div>

</body>

<script src="{{ asset('admin/js/jquery-3.6.0.min.js') }}"></script>
<script src="{{ asset('admin/js/bootstrap/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('admin/js/icons/feather-icon/feather.min.js') }}"></script>
<script src="{{ asset('admin/js/icons/feather-icon/feather-icon.js') }}"></script>
<script src="{{ asset('admin/js/config.js') }}"></script>
<script src="{{ asset('admin/js/script.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
@stack('scripts')

<script>
    document.addEventListener('livewire:initialized',()=>{

        Livewire.on('swal',(event)=>{
            const data=event
            swal.fire({
                icon:data[0]['icon'],
                title:data[0]['title'],
                text:data[0]['text'],
            })
        })

        Livewire.on('delete-prompt',(event)=>{
            swal.fire({
                title:'Are you sure?',
                text:'You are about to delete this record, this action is irreversible',
                icon:'warning',
                showCancelButton:true,
                confirmButtonColor:'#3085d6',
                showCancelButtonColor:'#d33',
                confirmButtonText:'Yes, Delete it!',
            }).then((result)=>{
                if(result.isConfirmed){
                    Livewire.dispatch('delete-now')

                    Livewire.on('deleted',(event)=>{
                       swal.fire({
                        title:'Deleted',
                        text:'Your record has been deleted',
                        icon:'success',
                       })
                    })
                }
            })
        })
    });


    window.addEventListener('validate:scroll-to', (ev) => {
        ev.stopPropagation();
        let selector = ev?.detail?.query;
        if (!selector) {
            return;
        }
        console.log(selector);
        $('html, body').animate({
            scrollTop: $(selector).offset().top - 50
        }, 1000);

    }, false);
</script>

</html>
